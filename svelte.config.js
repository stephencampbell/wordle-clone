import adapter from '@sveltejs/adapter-auto'
import { svelteSVG } from 'rollup-plugin-svelte-svg'

/** @type {import('@sveltejs/kit').Config} */
const config = {
  kit: {
    adapter: adapter(),

    // Override http methods in the Todo forms
    methodOverride: {
      allowed: ['PATCH', 'DELETE']
    },

    vite: {
      plugins: [
        svelteSVG({
          svgo: {},
          enforce: "pre"
        })
      ]
    }
  }
}

export default config
