# wordle

![Game screenshot](game-screenshot.png)

This app features a comprehensive word list with around 1/3 of a million words. These words are sorted in popularity order, which allows us to ensure only more popular words are chosen for answers.

Words of all lengths are included. Settings can be set in-game and are stored in local storage. The word service caches word lists so the app should still be relatively fast at runtime.

Thanks to [Norvig](https://norvig.com/ngrams/) for the words list! I wrote a short python script to parse it into JSON for use in this app.

This project was built using my own CSS framework, Blossom.

## Settings
- guessWordListSize - The number of most common words to allow for guesses.
- answerWordListSize - The number of most common words to use when picking the answer word.
- wordLength - The length of word to pick.
- numberOfGuesses - The number of guesses allowed before game over.
- letterOrder - Set to "qwerty" for a keyboard layout and "alphabet" for an alphabetical layout. On keyboard layout, letters are greyed out to preserve layout. On alphabetic layout, letters are instead removed when eliminated.

## Developing

Once you've created a project and installed dependencies with `npm install` (or `pnpm install` or `yarn`), start a development server:

```bash
npm run dev

# or start the server and open the app in a new browser tab
npm run dev -- --open
```

## Building

To create a production version of your app:

```bash
npm run build
```

You can preview the production build with `npm run preview`.

> To deploy your app, you may need to install an [adapter](https://kit.svelte.dev/docs/adapters) for your target environment.
