import { writable, derived, get } from 'svelte/store'
import words from '$lib/data/words'
import { wordLength, answerWordListSize, guessWordListSize } from '$lib/stores/settings'

export const game = writable(0)

export const wordsOfLength = derived(wordLength, $wordLength => words.filter(word => word.length === $wordLength))

export const guessWordsList = derived([wordsOfLength, guessWordListSize],
  ([$wordsOfLength, $guessWordListSize]) => $wordsOfLength.slice(0, Math.min($guessWordListSize, $wordsOfLength.length))
)

export const answerWordsList = derived([wordsOfLength, answerWordListSize],
  ([$wordsOfLength, $answerWordListSize]) => $wordsOfLength.slice(0, Math.min($answerWordListSize, $wordsOfLength.length))
)
export const word = derived([answerWordsList, game],
  ([$answerWordsList]) => $answerWordsList[Math.floor(Math.random() * (($answerWordsList.length - 1) + 1))]
)

export const isGuessInList = guess => get(guessWordsList).includes(guess.trim().toLowerCase())
export const getDefinitions = async () => {
  const res = await fetch(`https://api.dictionaryapi.dev/api/v2/entries/en/${get(word)}`)
  const data = await res.json()
  return data[0]?.meanings.map(meaning => `${meaning.partOfSpeech}: ${meaning.definitions[0].definition}`) ?? []
}
