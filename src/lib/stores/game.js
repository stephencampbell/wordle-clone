import { writable, get } from 'svelte/store'
import { numberOfGuesses } from './settings'
import { game } from './word'

export const guesses = writable([])

export function endGame() {
  const guessesNeeded = new Array(get(numberOfGuesses) - get(guesses).length)
  guesses.update(g => [...g, ...guessesNeeded])
}

export function newGame() {
  guesses.set([])
  game.update(g => g + 1)
}
