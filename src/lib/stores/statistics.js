import { get } from 'svelte/store'
import { writable } from './local-store'
import { guesses } from './game'
import { wordLength } from './settings'

export const dataByRoundsByNumberOfLetters = writable('winsByRoundsByNumberOfLetters', {})

// Adds a statistic based on current game state taken from the game store
export function addStatistic(didWin) {
  const key = didWin ? get(guesses).length : 'loss'
  const data = get(dataByRoundsByNumberOfLetters)
  const wordLengthValue = get(wordLength)

  if(!data[wordLengthValue]) data[wordLengthValue] = {}

  const val = data[wordLengthValue][key]
  if(!val) {
    data[wordLengthValue][key] = 1
  } else {
    data[wordLengthValue][key]++
  }

  dataByRoundsByNumberOfLetters.set(data)
}
