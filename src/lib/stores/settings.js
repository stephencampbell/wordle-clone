import { writable } from './local-store'

export const isDark = writable('isDark', false)
export const guessWordListSize = writable('guessWordListSize', 15000)
export const answerWordListSize = writable('answerWordListSize', 2500)
export const wordLength = writable('wordLength', 5)
export const numberOfGuesses = writable('numberOfGuesses', 6)
export const letterOrder = writable('letterOrder', 'qwerty')
