import { toast } from '@zerodevx/svelte-toast'

export default function pushToast(text) {
  toast.push(text, { dismissable: false, pausable: true	})
}
